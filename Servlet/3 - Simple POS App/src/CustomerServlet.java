import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(urlPatterns = "/customers")
public class CustomerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/absd", "root", "84260899");

            PreparedStatement pstm = connection.prepareStatement("SELECT * FROM Customer WHERE id=?");
            pstm.setObject(1, id);

            ResultSet rst = pstm.executeQuery();

            if (rst.next()){
                String name = rst.getString(2);
                String address = rst.getString(3);

                try(PrintWriter out = resp.getWriter()){
                    out.println("{\n" +
                            "  \"id\": \"\",\n" +
                            "  \"name\": \"\",\n" +
                            "  \"address\": \"\"\n" +
                            "}");
                }
            }else{
                try(PrintWriter out = resp.getWriter()){
                    out.println("{}");
                }
            }
        }catch (Exception ex){
            try(PrintWriter out = resp.getWriter()){
                out.println("{}");
            }
            ex.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String address = req.getParameter("address");

        boolean result = false;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/absd", "root", "84260899");

            PreparedStatement pstm = connection.prepareStatement("INSERT INTO Customer VALUES (?,?,?)");
            pstm.setObject(1,id);
            pstm.setObject(2, name);
            pstm.setObject(3,address);

            result = pstm.executeUpdate()> 0;

            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try(PrintWriter out = resp.getWriter()){
                out.println(result?"Customer has been saved successfully":"Failed to save the customer");
            }
        }

    }
}
